#!/bin/bash

cmd="$@"

rm b

echo "foo" > a
echo "making foo"
$cmd

sleep 0.01

echo "bar" > a
echo "making bar"
$cmd

[ `cat a` == `cat b` ] || echo "a != b" && exit 1
